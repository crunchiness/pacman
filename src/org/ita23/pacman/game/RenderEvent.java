package org.ita23.pacman.game;

import java.awt.*;
import java.io.IOException;

/**
 * Describes an object that can be rendered.
 * @author Lukas Knuth
 * @version 1.0
 */
public interface RenderEvent {


    /**
     * This method is used to draw the current state on the
     *  given {@code Graphics}-object.
     * @param g the object to draw an.
     * @return the drawn object.
     * @throws IOException 
     */
    public void render(Graphics g) throws IOException;


}
